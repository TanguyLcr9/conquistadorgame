﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDrag : MonoBehaviour
{

    public float dragSpeed = 1;
    private Vector3 dragOrigin;


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            dragOrigin = Input.mousePosition;
            return;
        }

        if (!Input.GetMouseButton(0)) return;

        Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
        Vector3 move = new Vector3(pos.x * dragSpeed, pos.y * dragSpeed,0f);

        transform.Translate(move, Space.World);

    }
    /*public Vector3 initPosition;
    public Vector2 offset;

    void FixedUpdate()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            initPosition = new Vector3(Input.mousePosition.x - transform.position.x, Input.mousePosition.y - transform.position.y, -10f);
        }
        if (Input.GetButton("Fire1"))
        {
            transform.position = (new Vector3((offset.x + Input.mousePosition.x - initPosition.x) * 0.01f, (offset.y + Input.mousePosition.y - initPosition.y) * 0.01f, 10f)) * -1f;
        }
        if (Input.GetButtonUp("Fire1"))
        {
            offset.x = transform.position.x;
            offset.y = transform.position.y;
        }
    }*/
}
