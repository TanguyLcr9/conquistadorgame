﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    public GameObject buildSlotUI , buildingUI , buildSlotSelected;

    public TextMeshProUGUI typeBuild, nameBuild;

    public int population, primaires, supplies;

    public List<BuildSlot> buildSlot = new List<BuildSlot>();
    public List<Building> buildingPrefab = new List<Building>();
    public List<Building> building = new List<Building>();
    // Start is called before the first frame update
    void Awake()
    {
        buildSlotUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
