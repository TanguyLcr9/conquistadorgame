﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildSlot : MonoBehaviour
{
    public GameManager gameManager;
    public Collider2D col;
    [HideInInspector]
    public bool onClicked;

    void Start() {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        col = GetComponent<Collider2D>();
    }

    void Update()
    {
        if (onClicked) //!Input.GetButtonDown("Fire1"))
        {
            ShowUI();
        }
    }

    virtual public void ShowUI()
    {
            gameManager.buildSlotUI.SetActive(true);
            foreach (BuildSlot slot in gameManager.buildSlot)
            {
                slot.col.enabled = false;
            }
        foreach (Building builds in gameManager.building)
        {
            builds.col.enabled = false;
        }
        onClicked = false;
        }

    void OnMouseDown() {//Active L'UI buildSlot et désactive tous les colliders des Slots
        onClicked = true;
        gameManager.buildSlotSelected = this.gameObject;
    }
}
