﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : BuildSlot
{
    [Range(0, 4)]
    public int type;
    private string[] NameBuild = { "Tipie des chasseurs", "Tipie des bûcherons", "Tipie de guerrier", "Tipie d'habitants", "Tipie du chef" };


    override public void ShowUI()
    {
        gameManager.typeBuild.text = "" + type;
        gameManager.nameBuild.text = NameBuild[type];
        
        gameManager.buildingUI.SetActive(true);
        foreach (BuildSlot slot in gameManager.buildSlot)
        {
            slot.col.enabled = false;
        }
        foreach (Building builds in gameManager.building)
        {
            builds.col.enabled = false;
        }
        onClicked = false;
    }
}
