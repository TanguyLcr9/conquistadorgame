﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingUI : MonoBehaviour
{
    public GameManager gameManager;
  

    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    public void ReturnOn() {//reactive les colliders et désactive l'UI 

        foreach (BuildSlot slot in gameManager.buildSlot)
        {
            slot.col.enabled = true;
        }
        foreach (Building builds in gameManager.building)
        {
            builds.col.enabled = true;
        }
        gameManager.buildSlotUI.SetActive(false);
        gameManager.buildingUI.SetActive(false);
        gameManager.buildSlotSelected = null;
    }

    public void CreateBuilding(int type)
    {
        Building instance = Instantiate(gameManager.buildingPrefab[type], gameManager.buildSlotSelected.transform.position, Quaternion.identity);
        instance.transform.SetParent(GameObject.Find("Board").transform);
        gameManager.building.Add(instance);
        gameManager.buildSlotSelected.SetActive(false);
        ReturnOn();

    }

}
